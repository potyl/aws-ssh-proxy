# Changelog

## 0.0.4
 - Add --proxy-url for providing the web proxy
 - If no arguments are given the tag names are printed

## 0.0.3
 - Add --auto-proxy for corporate environments
 - Fix crash if an EC2 instance has no tags

## 0.0.2
 - Initial release

## 0.0.1
 - Failed release
