.PHONY: build
build:
	rm -rf dist
	python setup.py sdist


.PHONY: upload-test
upload-test: build
	twine upload -r test dist/*


.PHONY: upload-pypi
upload-pypi: build
	twine upload -r pypi dist/*


.PHONY: clean
clean:
	rm -rf dist *.egg-info
